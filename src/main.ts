import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import { Context } from 'vm';
import { execSync } from "child_process";
const gitCommand = "git rev-parse HEAD";
import { getLastCommit } from "git-last-commit";

export default function getGitCommitHash()
{
  return execSync(gitCommand).toString().trim();
}

export async function gitinfo(ctx: Context, next: () => Promise<any>)
{
  ctx.body =
  {
    app: "BendigoBank",
    git: getGitCommit()
  };
  await next();
}

export default function getGitCommit()
{
  return new Promise((res, rej) =>
  {
    getLastCommit((err, commit) =>
    {
      if (err) return rej(err);
      return res(commit);
    });
  });
}

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
